package rs.milutinovicAleksandar.com.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.milutinovicAleksandar.com.app.model.Aerodrom;
import rs.milutinovicAleksandar.com.app.repository.AerodromRepository;

@Service
public class AerodromService {
	
	@Autowired
	AerodromRepository repository;
	
	public Iterable<Aerodrom> dobaviSve(){
		return repository.findAll();
	}
	
	public Aerodrom dobaviPoId(Long id) {
		return repository.findById(id).orElse(null);
	}
	
	public void save(Aerodrom obj) {
		repository.save(obj);
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}
	

}
