package rs.milutinovicAleksandar.com.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.milutinovicAleksandar.com.app.model.Let;

public interface LetRepository extends CrudRepository<Let, Long>{

}
