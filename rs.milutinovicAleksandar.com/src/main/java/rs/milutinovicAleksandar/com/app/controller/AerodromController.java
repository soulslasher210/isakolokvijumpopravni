package rs.milutinovicAleksandar.com.app.controller;

import java.util.ArrayList;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import rs.milutinovicAleksandar.com.app.dto.AerodromDTO;
import rs.milutinovicAleksandar.com.app.model.Aerodrom;
import rs.milutinovicAleksandar.com.app.service.AerodromService;

@Controller
@CrossOrigin
@RequestMapping(path = "/aerodrom")
public class AerodromController {
	@Autowired
	AerodromService service;
	ArrayList<AerodromDTO> list;
	
	
	//Dobavi sve
		@RequestMapping(path = "", method = RequestMethod.GET)
		public ResponseEntity<ArrayList<AerodromDTO>> dobaviSve(){
			ModelMapper mm = new ModelMapper();
			
			list = new ArrayList<AerodromDTO>();
			for(Aerodrom x:service.dobaviSve()) {
				list.add(mm.map(x, AerodromDTO.class));
			}
			
			return new ResponseEntity<ArrayList<AerodromDTO>>(list, HttpStatus.OK);
		}
		
		//Dobavi po ID
		@RequestMapping(path = "/{id}", method = RequestMethod.GET)
		public ResponseEntity<AerodromDTO> dobaviPoId(@PathVariable("id") Long id){
			ModelMapper mm = new ModelMapper();
			Aerodrom postojeci = service.dobaviPoId(id);
			
			if(postojeci == null) {
				return new ResponseEntity<AerodromDTO>(HttpStatus.NOT_FOUND);
			}
			AerodromDTO obj = mm.map(postojeci, AerodromDTO.class);
			return new ResponseEntity<AerodromDTO>(obj, HttpStatus.OK);
			
		}
		
		//Dodavanje novog
		@RequestMapping(path = "", method = RequestMethod.POST)
	    public ResponseEntity<Aerodrom> dodajNovi(@RequestBody Aerodrom obj) {
	        if (service.dobaviPoId(obj.getId()) != null) {
	            return new ResponseEntity<Aerodrom>(HttpStatus.CONFLICT);
	        }
	        service.save(obj);
	        return new ResponseEntity<Aerodrom>(HttpStatus.OK);
	    }
		
		//Izmena
	    @RequestMapping(path = "", method = RequestMethod.PUT)
	    public ResponseEntity<Aerodrom> izmeni(@RequestBody Aerodrom obj) {
	        Aerodrom postojeci = service.dobaviPoId(obj.getId());
	        
	        if (postojeci == null) {
	            return new ResponseEntity<Aerodrom>(HttpStatus.NOT_FOUND);
	        }
	        
	        
	        postojeci.setNaziv(obj.getNaziv());
	        postojeci.setSkracenica(obj.getSkracenica());
	        
	        
	        service.save(postojeci);
	        return new ResponseEntity<Aerodrom>(HttpStatus.OK);
	    }

	    //Brisanje
	    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> brisanje(@PathVariable("id") Long id) {
	    	
	        if (service.dobaviPoId(id) == null) {
	            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
	        }
	        
	        service.delete(id);
	        return new ResponseEntity<Object>(HttpStatus.OK);
	    }

}
