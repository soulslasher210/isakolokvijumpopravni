package rs.milutinovicAleksandar.com.app.dto;

import java.util.List;

public class AerodromDTO {
	
	private long id;
	private String naziv;
	private String skracenica;
	
	List<LetDTO>listaLetova;
	
	
	
	public List<LetDTO> getListaLetova() {
		return listaLetova;
	}
	public void setListaLetova(List<LetDTO> listaLetova) {
		this.listaLetova = listaLetova;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getSkracenica() {
		return skracenica;
	}
	public void setSkracenica(String skracenica) {
		this.skracenica = skracenica;
	}
	
	

}
