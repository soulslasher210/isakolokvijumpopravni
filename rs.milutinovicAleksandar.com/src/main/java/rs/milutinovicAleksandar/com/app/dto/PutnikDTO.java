package rs.milutinovicAleksandar.com.app.dto;

import java.util.List;

public class PutnikDTO {
	
	private long id;
	private String ime;
	private String prezime;
	
	
	List<LetDTO>listaLetova;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getIme() {
		return ime;
	}


	public void setIme(String ime) {
		this.ime = ime;
	}


	public String getPrezime() {
		return prezime;
	}


	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}


	public List<LetDTO> getListaLetova() {
		return listaLetova;
	}


	public void setListaLetova(List<LetDTO> listaLetova) {
		this.listaLetova = listaLetova;
	}
	
	

}
