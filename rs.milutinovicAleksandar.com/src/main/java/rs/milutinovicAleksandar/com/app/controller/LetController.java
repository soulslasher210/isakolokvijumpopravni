package rs.milutinovicAleksandar.com.app.controller;

import java.util.ArrayList;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



import rs.milutinovicAleksandar.com.app.dto.LetDTO;

import rs.milutinovicAleksandar.com.app.model.Let;
import rs.milutinovicAleksandar.com.app.service.LetService;



@Controller
@CrossOrigin
@RequestMapping(path = "/let")
public class LetController {
	
	LetService service;
	ArrayList<LetDTO> lista;
	
	
	
	//Dobavi sve
		@RequestMapping(path = "", method = RequestMethod.GET)
		public ResponseEntity<ArrayList<LetDTO>> dobaviSve(){
			ModelMapper mm = new ModelMapper();
			
			lista = new ArrayList<LetDTO>();
			for(Let x:service.dobaviSve()) {
				lista.add(mm.map(x, LetDTO.class));
			}
			
			return new ResponseEntity<ArrayList<LetDTO>>(lista, HttpStatus.OK);
		}
		
		//Dobavi po ID
		@RequestMapping(path = "/{id}", method = RequestMethod.GET)
		public ResponseEntity<LetDTO> dobaviPoId(@PathVariable("id") Long id){
			ModelMapper mm = new ModelMapper();
			Let postojeci = service.dobaviPoId(id);
			
			if(postojeci == null) {
				return new ResponseEntity<LetDTO>(HttpStatus.NOT_FOUND);
			}
			LetDTO obj = mm.map(postojeci, LetDTO.class);
			return new ResponseEntity<LetDTO>(obj, HttpStatus.OK);
			
		}
		
		//Dodavanje novog
		@RequestMapping(path = "", method = RequestMethod.POST)
	    public ResponseEntity<Let> dodajNovi(@RequestBody Let obj) {
	        if (service.dobaviPoId(obj.getId()) != null) {
	            return new ResponseEntity<Let>(HttpStatus.CONFLICT);
	        }
	        service.save(obj);
	        return new ResponseEntity<Let>(HttpStatus.OK);
	    }
		
		//Izmena
	    @RequestMapping(path = "", method = RequestMethod.PUT)
	    public ResponseEntity<Let> izmeni(@RequestBody Let obj) {
	        Let postojeci = service.dobaviPoId(obj.getId());
	        
	        if (postojeci == null) {
	            return new ResponseEntity<Let>(HttpStatus.NOT_FOUND);
	        }
	        
	        
	        postojeci.setCena(obj.getCena());
	        
	        
	        service.save(postojeci);
	        return new ResponseEntity<Let>(HttpStatus.OK);
	    }

	    //Brisanje
	    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> brisanje(@PathVariable("id") Long id) {
	    	
	        if (service.dobaviPoId(id) == null) {
	            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
	        }
	        
	        service.delete(id);
	        return new ResponseEntity<Object>(HttpStatus.OK);
	    }


}
