package rs.milutinovicAleksandar.com.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.milutinovicAleksandar.com.app.model.Putnik;

public interface PutnikRepository extends CrudRepository<Putnik, Long> {

}
