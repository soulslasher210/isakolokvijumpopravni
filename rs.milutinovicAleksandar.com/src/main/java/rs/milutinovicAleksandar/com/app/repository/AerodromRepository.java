package rs.milutinovicAleksandar.com.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.milutinovicAleksandar.com.app.model.Aerodrom;

public interface AerodromRepository extends CrudRepository<Aerodrom, Long> {

}
