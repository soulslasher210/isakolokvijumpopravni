package rs.milutinovicAleksandar.com.app.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToOne;



@Entity
public class Karta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	
	@Column(nullable = false)
	private LocalDateTime vremePolaska;
	@Column(nullable = false)
	private boolean povratna;
	@Column(nullable = false)
	private double cena;
	
	
	
	@OneToOne(mappedBy = "karta",fetch = FetchType.LAZY)
	List<Putnik> listaPutnika;



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public LocalDateTime getVremePolaska() {
		return vremePolaska;
	}



	public void setVremePolaska(LocalDateTime vremePolaska) {
		this.vremePolaska = vremePolaska;
	}



	public boolean isPovratna() {
		return povratna;
	}



	public void setPovratna(boolean povratna) {
		this.povratna = povratna;
	}



	public double getCena() {
		return cena;
	}



	public void setCena(double cena) {
		this.cena = cena;
	}



	public List<Putnik> getListaPutnika() {
		return listaPutnika;
	}



	public void setListaPutnika(List<Putnik> listaPutnika) {
		this.listaPutnika = listaPutnika;
	}



	public Karta(long id, LocalDateTime vremePolaska, boolean povratna, double cena, List<Putnik> listaPutnika) {
		super();
		this.id = id;
		this.vremePolaska = vremePolaska;
		this.povratna = povratna;
		this.cena = cena;
		this.listaPutnika = listaPutnika;
	}
	
	public Karta() {
		
	}
	

}
