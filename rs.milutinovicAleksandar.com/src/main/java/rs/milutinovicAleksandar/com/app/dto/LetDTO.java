package rs.milutinovicAleksandar.com.app.dto;

public class LetDTO {
	
	private Long id;
	private double cena;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	
	
	

}
