package rs.milutinovicAleksandar.com.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.milutinovicAleksandar.com.app.model.Karta;

public interface KartaRepository extends CrudRepository<Karta, Long> {

}
