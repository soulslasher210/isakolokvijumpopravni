package rs.milutinovicAleksandar.com.app.dto;

import java.time.LocalDateTime;
import java.util.List;



public class KartaDTO {
	
	private Long id;
	private LocalDateTime vremePolaska;
	private boolean povratna;
	private double cena;
	
	List<PutnikDTO>listaPutnika;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getVremePolaska() {
		return vremePolaska;
	}

	public void setVremePolaska(LocalDateTime vremePolaska) {
		this.vremePolaska = vremePolaska;
	}

	public boolean isPovratna() {
		return povratna;
	}

	public void setPovratna(boolean povratna) {
		this.povratna = povratna;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public List<PutnikDTO> getListaPutnika() {
		return listaPutnika;
	}

	public void setListaPutnika(List<PutnikDTO> listaPutnika) {
		this.listaPutnika = listaPutnika;
	}
	
	

}
