package rs.milutinovicAleksandar.com.app.service;

import org.springframework.beans.factory.annotation.Autowired;

import rs.milutinovicAleksandar.com.app.model.Putnik;
import rs.milutinovicAleksandar.com.app.repository.PutnikRepository;


public class PutnikService {
	
	@Autowired
	PutnikRepository repository;
	
	public Iterable<Putnik> dobaviSve(){
		return repository.findAll();
	}
	
	public Putnik dobaviPoId(Long id) {
		return repository.findById(id).orElse(null);
	}
	
	public void save(Putnik obj) {
		repository.save(obj);
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}

}
