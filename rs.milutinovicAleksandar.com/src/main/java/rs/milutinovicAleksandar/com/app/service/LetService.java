package rs.milutinovicAleksandar.com.app.service;



import org.springframework.beans.factory.annotation.Autowired;

import rs.milutinovicAleksandar.com.app.model.Let;
import rs.milutinovicAleksandar.com.app.repository.LetRepository;



public class LetService {
	@Autowired
	LetRepository repository;
	
	public Iterable<Let> dobaviSve(){
		return repository.findAll();
	}
	
	public Let dobaviPoId(Long id) {
		return repository.findById(id).orElse(null);
	}
	
	public void save(Let obj) {
		repository.save(obj);
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}
	
	

}
