package rs.milutinovicAleksandar.com.app.service;

import org.springframework.beans.factory.annotation.Autowired;


import rs.milutinovicAleksandar.com.app.model.Karta;

import rs.milutinovicAleksandar.com.app.repository.KartaRepository;

public class KartaService {
	
	
	@Autowired
	KartaRepository repository;
	
	public Iterable<Karta> dobaviSve(){
		return repository.findAll();
	}
	
	public Karta dobaviPoId(Long id) {
		return repository.findById(id).orElse(null);
	}
	
	public void save(Karta obj) {
		repository.save(obj);
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}
	
	

}
