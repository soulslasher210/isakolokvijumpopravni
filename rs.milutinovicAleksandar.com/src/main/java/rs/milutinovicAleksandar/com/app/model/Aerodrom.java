package rs.milutinovicAleksandar.com.app.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;



@Entity
public class Aerodrom {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false)
	private String naziv;
	@Column(nullable = false)
	private String skracenica;
	
	
	@OneToMany(mappedBy = "Aerodrom" ,fetch = FetchType.LAZY)
	List<Let> let;
	
	
	public Aerodrom(long id, String naziv, String skracenica, List<Let> let) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.skracenica = skracenica;
		this.let = let;
	}
	
	public Aerodrom() {
		
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public String getSkracenica() {
		return skracenica;
	}


	public void setSkracenica(String skracenica) {
		this.skracenica = skracenica;
	}


	public List<Let> getLet() {
		return let;
	}


	public void setLet(List<Let> let) {
		this.let = let;
	}


	
	

}
